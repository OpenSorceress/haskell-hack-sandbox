# Haskell Basics

* What is Haskell?
* Course Themes
* Declarations and variables
* Basic Types
* GHCi
* Arithmetic
* Boolean logic
* Defining basic functions
* Pairs
* Using functions, and multiple arguments
* Lists
* Combining functions
* A word on error messages

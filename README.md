# CIS194 - Spring 2013

## Introduction

Welcome! This is an unofficial repackaging of the materials for cis194
for easier setup. I've not made any modifications to the materials.

Here's the original course home page:
[link](http://www.cis.upenn.edu/~cis194/spring13/)

For those not familiar with cis194, it's an excellent introduction to
the [Haskell](https://www.haskell.org/) programming language. The
lecture notes are fun to read, and cover many of the core abstractions
used when interacting with existing Haskell libraries.

## Prerequisites

cis194 works best if you've programmed before, at least a little. It
assumes that:

* You have GHC/Haskell installed
  * If not, go [here](https://www.haskell.org/downloads)
* You're comfortable enough in a command line interface that you can:
    * run the GHC/Haskell intepreter (ghci)
    * run the GHC/Haskell compiler (ghc)
    * edit source files

## Organization

Materials are split across weeks, with the following layout:

```
week-N:
  - N-<title>.lhs -- lecture notes
  - N-<titel>.pdf -- homework spec
  - src/          -- homework helper/starter code
  - data/         -- homework data files
```

## Getting Started

To get started with this course:

1. `git clone git@gitlab.com:OpenSorceress/haskell-hack-sandbox.git`
2. `git checkout -b <your-name>`
3. Start working through the notes and exercises!

Please always make sure you are checking in to your branch.  You should never make any changes on the `master` branch.

When you want to publish your changes, be sure to check them in and then

4. `git push`

If this is your first time publishing changes to this repository, or if you have created a new branch, git will reply with extra instructions to create the remote copy of the branch.  Just modify the `git push` command to fit the instructions git gives you.  For instance:

4. `git push --set-upstream origin <your-name>`

## Course Overview

* Week 01: Haskell Basics
* Week 02: Algebraic Data Types
* Week 03: Recursion Patterns. Polymorphism, and the Prelude
* Week 04: Higher-Order Programming and Type Inference
* Week 05: More Polymorphism and Type Classes
* Week 06: Lazy Evaluation
* Week 07: Folds and Monoids
* Week 08: IO
* Week 09: Functors
* Week 10: Applicative Functors I
* Week 11: Applicative Functors II
* Week 12: Monads
